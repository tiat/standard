<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Parameters;

//
use ArrayAccess;
use Countable;
use Serializable;
use Traversable;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface ParametersInterface extends ArrayAccess, Countable, Serializable, Traversable {
	
	/**
	 * @param    array    $values
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(array $values);
	
	/**
	 * From array
	 * Allow deserialization from standard array
	 *
	 * @param    array    $values
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function fromArray(array $values) : void;
	
	/**
	 * From string
	 * Allow deserialization from raw body; e.g., for PUT requests
	 *
	 * @param    string    $string
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function fromString(string $string) : void;
	
	/**
	 * To array
	 * Allow serialization back to standard array
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : array;
	
	/**
	 * To string
	 * Allow serialization to query format; e.g., for PUT or POST requests
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string;
	
	/**
	 * @param    string    $name
	 * @param    mixed     $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function get(string $name, mixed $default) : mixed;
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return ParametersInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function set(string $name, mixed $value) : ParametersInterface;
}
