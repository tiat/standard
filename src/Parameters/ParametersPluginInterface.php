<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Parameters;

//
use ArrayAccess;
use Tiat\Standard\Plugin\PluginInterface;

/**
 * @version 3.1.1
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface ParametersPluginInterface extends ArrayAccess, PluginInterface {
	
	/**
	 * @return bool
	 * @since   3.1.1 First time introduced.
	 */
	public function getCaseInsensitive() : bool;
	
	/**
	 * @param    bool    $caseInsensitive
	 *
	 * @return ParametersPluginInterface
	 * @since   3.1.1 First time introduced.
	 */
	public function setCaseInsensitive(bool $caseInsensitive) : ParametersPluginInterface;
	
	/**
	 * @param    string|int    $key
	 * @param    null|mixed    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getParam(string|int $key, mixed $default) : mixed;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getParams() : ?array;
	
	/**
	 * Does param exists
	 *
	 * @param    string|int    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasParam(string|int $key) : bool;
	
	/**
	 * @param    ArrayAccess|iterable    $params
	 * @param    bool                    $override           Override param if it exists already
	 * @param    bool                    $paramValidation    Override param validation
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParams(ArrayAccess|iterable $params, bool $override, bool $paramValidation) : ParametersPluginInterface;
	
	/**
	 * @param    string|int    $key
	 * @param    mixed|NULL    $value
	 * @param    bool          $override           Override param if it exists already
	 * @param    bool          $paramValidation    Override single param validation required
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParam(string|int $key, mixed $value, bool $override, bool $paramValidation, bool $throwError) : ParametersPluginInterface;
	
	/**
	 * Set param value to null
	 *
	 * @param    string|int    $key
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetParam(string|int $key) : ParametersPluginInterface;
	
	/**
	 * Set param validation ON (true) or OFF (false)
	 *
	 * @param    bool    $status
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParamEncodingValidation(bool $status) : ParametersPluginInterface;
	
	/**
	 * Is param encoding validation required/performed
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getParamEncodingValidation() : bool;
	
	/**
	 * Delete param
	 *
	 * @param    string|int    $key
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteParam(string|int $key) : ParametersPluginInterface;
	
	/**
	 * Get used encoding
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : ?string;
	
	/**
	 * Check if strings are valid for the specified encoding
	 * Try to prevent "Invalid Encoding Attack"
	 *
	 * @param    array|string    $value
	 * @param    null|string     $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEncoding(array|string $value, ?string $encoding) : bool;
	
	/**
	 * Set used encoding
	 *
	 * @param    string    $encoding
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : ParametersPluginInterface;
	
	/**
	 * Reset encoding to default
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncoding() : ParametersPluginInterface;
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetGet(mixed $offset) : mixed;
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetExists(mixed $offset) : bool;
	
	/**
	 * @param    mixed    $offset
	 * @param    mixed    $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetSet(mixed $offset, mixed $value) : void;
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetUnset(mixed $offset) : void;
}
