<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Reader;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ReaderInterface {
	
	/**
	 * Read from a file and create an array. Method will return false if failure.
	 *
	 * @param    string    $filename
	 *
	 * @return array|false
	 * @since   3.0.0 First time introduced.
	 */
	public function fromFile(string $filename) : array|false;
	
	/**
	 * Read from a string and create an array. Method will return false if failure.
	 *
	 * @param    string    $string
	 *
	 * @return array|bool
	 * @since   3.0.0 First time introduced.
	 */
	public function fromString(string $string) : array|false;
}
