<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Config;

//
use ArrayAccess;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface ConfigFileInterface extends ArrayAccess, ClassLoaderInterface {
	
	/**
	 * Construct the config file interface
	 *
	 * @param    iterable    $params
	 * @param    bool        $autoinit    Autoinit the class with init()
	 * @param    bool        $autorun     Autorun the class with run()
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params, bool $autoinit = TRUE, bool $autorun = TRUE, ...$args);
	
	/**
	 * Also calling as function is allowed
	 *
	 * @param    iterable    $params
	 * @param                ...$args
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function __invoke(iterable $params, ...$args) : mixed;
	
	/**
	 * Register config file config
	 *
	 * @return ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerConfig() : ConfigFileInterface;
	
	/**
	 * Get register interface
	 *
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterInterface() : ?RegisterInterface;
	
	/**
	 * Set register interface
	 *
	 * @param    RegisterInterface    $register
	 *
	 * @return ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterInterface(RegisterInterface $register) : ConfigFileInterface;
	
	/**
	 * Delete register interface
	 *
	 * @return ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRegisterInterface() : ConfigFileInterface;
	
	/**
	 * Register all values at once to Register interface
	 *
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    array                                                $values
	 * @param    array                                                $options
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function registerConfigValues(RegisterPluginInterface&ParametersPluginInterface $register, array $values, array $options = []) : void;
	
	/**
	 * Merge config values (use with config values from default config & application)
	 *
	 * @param    null|mixed    $default
	 * @param    array         $args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function mergeConfigValues(array $default, array $args = []) : array;
}
