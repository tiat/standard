<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Config;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface ConfigLoaderInterface {
	
	/**
	 * @return null|ConfigInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfig() : ?ConfigInterface;
	
	/**
	 * @param    ConfigInterface    $config
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfig(ConfigInterface $config) : ConfigLoaderInterface;
	
	/**
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfig() : ConfigLoaderInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkConfigFile() : bool;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigFile() : ?string;
	
	/**
	 * @param    string    $filename
	 * @param    bool      $autoload    If file is class and supports autoload
	 * @param    bool      $overwrite
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfigFile(string $filename, bool $autoload = TRUE, bool $overwrite = FALSE) : ConfigLoaderInterface;
	
	/**
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfigFile() : ConfigLoaderInterface;
	
	/**
	 * @param    string|iterable    $source
	 * @param    NULL|callable      $handler
	 *
	 * @return null|ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings(string|iterable $source, ?callable $handler) : ?ConfigLoaderInterface;
	
	/**
	 * @param    string    $source
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsFromClass(string $source) : ConfigLoaderInterface;
	
	/**
	 * @param    iterable|string    $source
	 * @param    NULL|callable      $handler
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsFromFile(iterable|string $source, ?callable $handler) : ConfigLoaderInterface;
}
