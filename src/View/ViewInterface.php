<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\View;

//
use Countable;

/**
 * The View helper is used to render a specified template within its own variable scope.
 * The primary use is for reusable template fragments with which you do not need to worry about variable name clashes.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ViewInterface extends Countable, ViewRegisterInterface {
	
	/**
	 * Add a child View
	 *
	 * @param    ViewInterface    $child
	 * @param    string           $capture
	 *
	 * @return ViewInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addChild(ViewInterface $child, string $capture) : ViewInterface;
	
	/**
	 * Get all child's
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getChild() : ?array;
	
	/**
	 * Does the View have any child
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasChild() : bool;
	
	/**
	 * Set the name of the variable to which to capture this model
	 *
	 * @param    string    $capture
	 *
	 * @return ViewInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCapture(string $capture) : ViewInterface;
	
	/**
	 * Get the name of the variable to which to capture this model
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCapture() : ?string;
	
	/**
	 * Set the template to be used by this helper
	 *
	 * @param    string    $template
	 *
	 * @return ViewInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTemplate(string $template) : ViewInterface;
	
	/**
	 * Get the template to be used by this helper
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTemplate() : ?string;
	
	/**
	 * Set the priority of the View
	 *
	 * @param    ViewPriority    $viewPriority
	 *
	 * @return ViewInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPriority(ViewPriority $viewPriority) : ViewInterface;
	
	/**
	 * Get the priority of the View
	 *
	 * @return null|ViewPriority
	 * @since   3.0.0 First time introduced.
	 */
	public function getPriority() : ?ViewPriority;
	
	/**
	 * Set the name of the View
	 *
	 * @param    string    $name
	 *
	 * @return ViewInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : ViewInterface;
	
	/**
	 * Get the name of the View
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * Render the template & set content to Capture
	 *
	 * @param    string    $file
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function render(string $file) : bool;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function __toString() : string;
}
