<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\View;

//
use ArrayAccess;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ViewRegisterInterface {
	
	/**
	 * Set variable for the View
	 *
	 * @param    string    $key
	 * @param    mixed     $val
	 * @param    bool      $overwrite
	 *
	 * @return ViewRegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParam(string $key, mixed $val, bool $overwrite = FALSE) : ViewRegisterInterface;
	
	/**
	 * Set variables for the View en masse
	 *
	 * @param    ArrayAccess|iterable    $params
	 * @param    bool                    $overwrite
	 *
	 * @return ViewRegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParams(ArrayAccess|iterable $params, bool $overwrite = FALSE) : ViewRegisterInterface;
	
	/**
	 * Get a single variable for the View
	 *
	 * @param    string        $name
	 * @param    mixed|NULL    $default    Default value if the variable is not present.
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getParam(string $name, mixed $default) : mixed;
	
	/**
	 * Get the View params
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getParams() : array;
	
	/**
	 * Set an option for the View
	 *
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return ViewRegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOption(string $name, mixed $value) : ViewRegisterInterface;
	
	/**
	 * Set options for the View en masse
	 *
	 * @param    iterable    $options
	 *
	 * @return ViewRegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOptions(iterable $options) : ViewRegisterInterface;
	
	/**
	 * Get the View options
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getOptions() : array;
	
	/**
	 * Reset the View options
	 *
	 * @return ViewRegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetOptions() : ViewRegisterInterface;
}
