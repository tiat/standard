<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\View;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum ViewPriority: int implements InterfaceEnum, InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	// Order is important when getting values with self::cases()
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HIGH = 3;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case MEDIUM = 2;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case LOW = 1;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case NONE = 0;
}
