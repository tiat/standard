<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Pipeline;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface PipelineInterface {
	
	/**
	 * Return the amount of pipe(s) in the pipeline
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function checkPipeline() : int;
	
	/**
	 * Register all pipe actions at once
	 *
	 * @param    array    $actions
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipeActions(array $actions) : PipelineInterface;
	
	/**
	 * @param    string    $action
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipeAction(string $action) : PipelineInterface;
	
	/**
	 * @param    string    $action
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deletePipeAction(string $action) : PipelineInterface;
	
	/**
	 * Register the callable ($pipe) to the pipeline
	 *
	 * @param    null|callable        $pipe
	 * @param    null|string          $method     Method name if $pipeline is object where you like to run the named method
	 * @param    array                $actions    Give all actions at once. Please notice that then it's your job to make sure that all actions are defined.
	 * @param                         ...$args
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipe(callable|null $pipe, ?string $method = NULL, array $actions = [], ...$args) : PipelineInterface;
	
	/**
	 * Remove callable ($pipe) from pipeline
	 *
	 * @param    callable    $pipe
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function unregisterPipe(callable $pipe) : PipelineInterface;
	
	/**
	 * Find the pipe with string (name) or object
	 *
	 * @param    object|string    $pipe
	 *
	 * @return null|object
	 * @since   3.0.0 First time introduced.
	 */
	public function findPipe(object|string $pipe) : ?object;
	
	/**
	 * Find the pipe by name (string; shortname from object as default or user defined)
	 *
	 * @param    string    $name
	 *
	 * @return null|object
	 * @since   3.0.0 First time introduced.
	 */
	public function findPipeByName(string $name) : ?object;
	
	/**
	 * Resolve the pipe name from callable (shortname from object)
	 *
	 * @param    callable|object|string    $pipe
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function resolvePipeName(callable|object|string $pipe) : ?string;
	
	/**
	 * Get all given pipe actions
	 *
	 * @param    object|string    $pipe
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeActions(object|string $pipe) : ?array;
	
	/**
	 * Get the single pipe action value
	 *
	 * @param    object|string    $pipe
	 * @param    string           $action
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeAction(object|string $pipe, string $action) : mixed;
	
	/**
	 * Set all pipe actions at once
	 *
	 * @param    object|string    $pipe
	 * @param    array            $actions
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeActions(object|string $pipe, array $actions = []) : PipelineInterface;
	
	/**
	 * Set a single pipe action
	 *
	 * @param    object|string    $pipe
	 * @param    string           $name
	 * @param    mixed            $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeAction(object|string $pipe, string $name, mixed $value) : PipelineInterface;
	
	/**
	 * Do things BEFORE the fiber will be started. Pipeline will set $pipe object as variable for closures.
	 * Example: $this->before( 'objectName', function ($var) { var_dump($var); }); will dump information of object.
	 *
	 * @param    callable|object|string    $pipe
	 * @param    callable                  $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function before(callable|object|string $pipe, callable $value) : PipelineInterface;
	
	/**
	 * Do things AFTER the fiber has been terminated, or it's not running anymore. Pipeline will set $pipe object as variable for closures.
	 * Example: $this->after( 'objectName', function ($var) { var_dump($var); }); will dump information of object.
	 *
	 * @param    callable|object|string    $pipe
	 * @param    callable                  $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function after(callable|object|string $pipe, callable $value) : PipelineInterface;
	
	/**
	 * Get the result from named pipe
	 *
	 * @param    object|string    $pipe
	 * @param    mixed            $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult(object|string $pipe, mixed $default) : mixed;
	
	/**
	 * Has pipeline already run or not
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipelineRunStatus() : bool;
	
	/**
	 * Set the pipeline run status (automatically modified by run())
	 *
	 * @param    bool    $status
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipelineRunStatus(bool $status) : static;
}
