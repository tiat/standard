<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Pipeline;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum PipelineVars: string implements InterfaceEnum, InterfaceEnumString {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_AFTER = 'PIPE_ACTION_AFTER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_ARGS = 'PIPE_ACTION_ARGS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_BEFORE = 'PIPE_ACTION_BEFORE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_FIBER = 'PIPE_ACTION_FIBER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_METHOD = 'PIPE_ACTION_METHOD';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_NAME = 'PIPE_ACTION_NAME';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_RESULT = 'PIPE_ACTION_RESULT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_RESULT_AFTER = 'PIPE_ACTION_RESULT_AFTER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_RESULT_BEFORE = 'PIPE_ACTION_RESULT_BEFORE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_STATUS = 'PIPE_ACTION_STATUS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_ACTION_SUSPEND = 'PIPE_ACTION_SUSPEND';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PIPE_MULTIPLE_EXECUTION = 'PIPE_MULTIPLE_EXECUTION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const PipelineVars PIPE_ACTION_RESULT_PIPE = self::PIPE_ACTION_RESULT;
}
