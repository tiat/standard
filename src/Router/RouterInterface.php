<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Router;

//
use Psr\Http\Message\RequestInterface;
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Router\Std\RouterModel;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterInterface {
	
	/**
	 * Shutdown for the Router
	 * Collect all events inside the router and save them at once to log
	 * when script execution finishes or exit() is called
	 *
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void;
	
	/**
	 * Get the router model
	 *
	 * @return RouterModel
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterModel() : RouterModel;
	
	/**
	 * Set router model settings in array
	 *
	 * @param    array    $routerModel
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterModel(array $routerModel) : RouterInterface;
	
	/**
	 * Reset the router type
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouterModel() : RouterInterface;
	
	/**
	 * Add new route map.
	 *
	 * @param    RouteParams     $name
	 * @param    MapInterface    $map
	 *
	 * @return void
	 */
	public function addRouteMap(RouteParams $name, MapInterface $map) : void;
	
	/**
	 * Get current route with defined Router type
	 * - MVC will return array
	 * - View will return string
	 * - For other types please check the docs
	 *
	 * @param    RouterModel    $route
	 *
	 * @return array|string
	 * @since   3.0.0 First time introduced.
	 * @see     RouterModel
	 */
	public function getRoute(RouterModel $route) : array|string;
	
	/**
	 * @param    string    $route    Valid URI query
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRoute(string $route) : RouterInterface;
	
	/**
	 * Reset the route
	 *
	 * @param    string    $route
	 *
	 * @return RouterInterface
	 */
	public function resetRoute(string $route) : RouterInterface;
	
	/**
	 * Get the request interface
	 *
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequest() : ?RequestInterface;
	
	/**
	 * Set the request interface
	 *
	 * @param    RequestInterface    $request
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequest(RequestInterface $request) : RouterInterface;
	
	/**
	 * Reset the request interface
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRequest() : RouterInterface;
	
	/**
	 * Get language settings.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguageSettings() : array;
	
	/**
	 * Get language settings with key.
	 *
	 * @param    string    $key
	 *
	 * @return mixed
	 */
	public function getLanguageSetting(string $key) : mixed;
	
	/**
	 * Set new language settings.
	 *
	 * @param    array    $settings
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguageSettings(array $settings) : RouterInterface;
	
	/**
	 * Get used language.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguage() : ?array;
	
	/**
	 * Set new language. If array then array order is followed.
	 *
	 * @param    string|array    $lang
	 * @param    string          $default
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguage(string|array $lang, string $default) : RouterInterface;
	
	/**
	 * Reset language to default.
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetLanguage() : RouterInterface;
}
