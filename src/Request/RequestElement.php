<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Request;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RequestElement: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	// PSR-7 based specification
	
	/**
	 * The authority component of the URL, which includes the user information, host, and port.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case AUTHORITY = 'authority';
	
	/**
	 * The fragment component of the URL, which follows the '#' symbol and identifies a secondary resource.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case FRAGMENT = 'fragment';
	
	/**
	 * The host component of the URL, which identifies the domain or IP address.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case HOST = 'host';
	
	/**
	 * The path component of the URL, which specifies the hierarchical part of the URL.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PATH = 'path';
	
	/**
	 * The port component of the URL, specifying the communication endpoint.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PORT = 'port';
	
	/**
	 * The query component of the URL, which includes the query string typically used for passing parameters.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case QUERY = 'query';
	
	/**
	 * The scheme component of the URL, such as 'http' or 'https', which defines the protocol.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case SCHEME = 'scheme';
	
	/**
	 * The user information component of the URL, which includes username and password, if provided.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case USERINFO = 'userinfo';
	
	/**
	 * Custom HTTP method (not PSR-7 compliant)
	 * Example: https://example.com/my/path?param1=value1:custom_method
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case METHOD = 'method';
}
