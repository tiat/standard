<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Request;

//
use Psr\Http\Message\RequestInterface as PSRRequestInterface;
use Psr\Http\Message\UriInterface;
use Tiat\Standard\DataModel\HttpMethodCustom;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RequestInterface extends PSRRequestInterface {
	
	/**
	 * Set the HTTP method of the request. Can be official HTTP method or custom
	 *
	 * @param    string    $method
	 *
	 * @return RequestInterface
	 * @see     HttpMethodCustom
	 * @since   3.0.0 First time introduced.
	 */
	public function setMethod(string $method) : RequestInterface;
	
	/**
	 * @param    UriInterface    $uri
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setUri(UriInterface $uri) : RequestInterface;
	
	/**
	 * @param    array    $originals
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeaders(array $originals) : RequestInterface;
}
