<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Monitor;

/**
 * This interface will provide basic functionality for monitoring inside the application.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface MonitorInterface {
	
	/**
	 * Has monitoring been started.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasStarted() : bool;
	
	/**
	 * Has monitoring been stopped.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasStopped() : bool;
	
	/**
	 * Set monitoring status.
	 *
	 * @param    bool    $status
	 *
	 * @return MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setStarted(bool $status) : MonitorInterface;
	
	/**
	 * @param    bool    $status
	 *
	 * @return MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setStopped(bool $status) : MonitorInterface;
	
	/**
	 * Start monitoring
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function start() : void;
	
	/**
	 * Stop monitoring
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function stop() : void;
	
	/**
	 * Get monitoring result
	 *
	 * @return null|string|array|int|false
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : string|array|int|false|null;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResult() : void;
	
	/**
	 * Get name for the monitoring instance
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * Set name for the monitoring instance
	 *
	 * @param    string    $name
	 *
	 * @return MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : MonitorInterface;
	
}
