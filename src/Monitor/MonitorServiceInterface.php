<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Monitor;

//
use Jantia\Standard\Version\VersionPackageInterface;

/**
 * Monitor as a service
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface MonitorServiceInterface extends MonitorInterface {
	
	/**
	 * @return null|VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPackageInterface() : ?VersionPackageInterface;
	
	/**
	 * Use given interface or create new with defined default interface & give args for new interface.
	 *
	 * @param    VersionPackageInterface    $interface
	 * @param    mixed                      ...$args
	 *
	 * @return MonitorServiceInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPackageInterface(VersionPackageInterface $interface, ...$args) : MonitorServiceInterface;
	
	/**
	 * @return MonitorServiceInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetPackageInterface() : MonitorServiceInterface;
}
