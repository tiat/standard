<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Monitor;

/**
 * Each metrics should implement this class (metrics = KPI etc.)
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/metrics
 */
interface MonitorMetricsInterface extends MonitorInterface {
	
	/**
	 * Get the name of the metrics.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * Set the name of the metrics.
	 *
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : void;
}
