<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Time;

//
use DateTimeImmutable;
use DateTimeZone;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface TimestampInterface {
	
	/**
	 * Get the used timezone
	 *
	 * @return null|DateTimeZone
	 * @since   3.0.0 First time introduced
	 */
	public function getTimezone() : ?DateTimeZone;
	
	/**
	 * Set the used timezone
	 *
	 * @param    DateTimeZone    $timeZone
	 *
	 * @return TimestampInterface
	 * @since   3.0.0 First time introduced
	 */
	public function setTimezone(DateTimeZone $timeZone) : TimestampInterface;
	
	/**
	 * Set the used time format
	 *
	 * @param    string    $format
	 *
	 * @return TimestampInterface
	 * @since   3.0.0 First time introduced
	 */
	public function setTimeFormat(string $format) : TimestampInterface;
	
	/**
	 * Get the used time format
	 *
	 * @return string
	 * @since   3.0.0 First time introduced
	 */
	public function getTimeFormat() : string;
	
	/**
	 * @param    string    $format
	 * @param    string    $datetime
	 *
	 * @return string
	 * @since   3.0.0 First time introduced
	 */
	public function createFrom(string $format, string $datetime) : string;
	
	/**
	 * Returns a new instance of DateTimeImmutable based on the provided timestamp and timezone. If no timestamp is provided, it uses the internal timestamp of the class. If no timezone is provided, it uses the internal timezone of the class.
	 *
	 * @param    string          $timestamp    The timestamp to create the DateTimeImmutable instance with. Defaults to the internal timestamp if not provided.
	 * @param    DateTimeZone    $timezone     The timezone to create the DateTimeImmutable instance with. Defaults to the internal timezone if not provided.
	 *
	 * @return DateTimeImmutable A new instance of DateTimeImmutable.
	 * @since 3.0.0 First time introduced.
	 */
	public function getDateTimeImmutable(string $timestamp, DateTimeZone $timezone) : DateTimeImmutable;
}
