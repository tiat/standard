<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Register;

//
use Tiat\Stdlib\Register\RegisterContainerInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RegisterPluginInterface {
	
	/**
	 * Get all config params in one array.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterKeyAll() : ?array;
	
	/**
	 * Get register value
	 *
	 * @param    string|int|object    $key
	 * @param    mixed                $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterKey(string|int|object $key, mixed $default) : mixed;
	
	/**
	 * Set all config params at once. Overwrite current values if exists (default: false).
	 *
	 * @param    array    $config
	 * @param    bool     $overwrite
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterKeyAll(array $config, bool $overwrite = FALSE) : RegisterPluginInterface;
	
	/**
	 * Set register value. Overwrite current values if exists (default: false).
	 *
	 * @param    string|int|object    $key
	 * @param    mixed                $value
	 * @param    bool                 $overwrite
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterKey(string|int|object $key, mixed $value, bool $overwrite = FALSE) : RegisterPluginInterface;
	
	/**
	 * Check if config key already exists.
	 *
	 * @param    string|int|object    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkRegisterKey(string|int|object $key) : bool;
	
	/**
	 * @param    string|int|object    $key
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRegisterKey(string|int|object $key) : RegisterPluginInterface;
	
	/**
	 * @param    string|int|object    $key
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRegisterKey(string|int|object $key) : RegisterPluginInterface;
	
	/**
	 * Get all register containers.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterContainers() : ?array;
	
	/**
	 * Get the register container by its name
	 *
	 * @param    string    $name
	 *
	 * @return null|RegisterContainerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterContainer(string $name) : ?RegisterContainerInterface;
	
	/**
	 * Sets the register container.
	 *
	 * @param    RegisterContainerInterface    $container    The register container to be set.
	 *
	 * @return RegisterPluginInterface The updated instance of the
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterContainer(RegisterContainerInterface $container) : RegisterPluginInterface;
}
