<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Register;

//
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RegisterInterface {
	
	/**
	 * Set & register new register. If you want to limit access to register then use passphrase.
	 *
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    mixed                                                $value
	 * @param    bool                                                 $overwrite
	 * @param    null|string                                          $passphrase
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public static function setRegister(RegisterPluginInterface&ParametersPluginInterface $register, mixed $value, bool $overwrite = FALSE, ?string $passphrase = NULL) : void;
	
	/**
	 * Get the registered register by name or with object.
	 *
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 * @param    null|string                                                   $passphrase
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public static function getRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register, ?string $passphrase = NULL) : ( RegisterPluginInterface&ParametersPluginInterface )|null;
	
	/**
	 * Check if the register is already registered.
	 *
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public static function checkRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register) : bool;
	
	/**
	 * Delete the register from register tree with all content. If you want to reset register then you have to delete it
	 * before or overwrite. Passphrase is needed if it's defined.
	 *
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 * @param    null|string                                                   $passphrase
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public static function deleteRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register, ?string $passphrase = NULL) : void;
	
	/**
	 * Check from the register that if register already exists or return same instance back.
	 *
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    null|string                                          $passphrase
	 *
	 * @return RegisterPluginInterface&ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public static function pullRegister(RegisterPluginInterface&ParametersPluginInterface $register, ?string $passphrase = NULL) : RegisterPluginInterface&ParametersPluginInterface;
}
