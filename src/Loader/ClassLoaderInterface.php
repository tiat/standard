<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Loader;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/standard
 */
interface ClassLoaderInterface {
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getAutoinit() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setAutoinit(bool $status) : ClassLoaderInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getAutorun() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setAutorun(bool $status) : ClassLoaderInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isInitCompleted() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setInitCompleted(bool $status) : ClassLoaderInterface;
	
	/**
	 * Check that is the init() process completed. If $init is true then init must be completed before process can continue.
	 *
	 * @param    bool    $init
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkInit(bool $init) : bool;
	
	/**
	 * Has class/module already run
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasRun() : bool;
	
	/**
	 * Set new run status for class/module
	 *
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRun(bool $status) : ClassLoaderInterface;
	
	/**
	 * Initialize the interface ready for process execution.
	 *
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ClassLoaderInterface;
	
	/**
	 * Execute the process.
	 *
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface;
	
	/**
	 * Set settings with magic style to target object calling setters.
	 *
	 * @param    callable    $obj
	 * @param    array       $settings
	 *
	 * @return void
	 */
	public function setSettings(callable $obj, array $settings) : void;
}
