<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Uri;

//
use Psr\Http\Message\UriInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface UriFileInterface extends UriInterface {
	
	/**
	 * Convert a UNIX file path to a valid file:// URL
	 *
	 * @param    string    $path
	 *
	 * @return UriFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public static function fromUnixPath(string $path) : UriFileInterface;
	
	/**
	 * Convert a Windows file path to a valid file:// URL
	 *
	 * @param    string    $path
	 *
	 * @return UriFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public static function fromWindowsPath(string $path) : UriFileInterface;
}
