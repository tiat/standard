<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\Identifiers;

//
use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;
use Tuupola\Base32;

/**
 * This interface defines the methods for generating and manipulating identifiers like RFC 4122 UUID and ULID.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface IdentifiersInterface {
	
	/**
	 * Create a ULID (Universally Unique Lexicographically Sortable Identifier)
	 * and return the string representation of the ULID.
	 *
	 * @param    UuidInterface    $uuid    The UUID version 7 interface
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function createUlid(UuidInterface $uuid) : string;
	
	/**
	 * Return the UUID instance if available, otherwise null.
	 *
	 * @return null|UuidInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getUuid() : ?UuidInterface;
	
	/**
	 * Set the UUID instance.
	 *
	 * @param    UuidInterface    $uuid
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setUuid(UuidInterface $uuid) : void;
	
	/**
	 * Return the UUID version if available, otherwise null.
	 *
	 * @return null|int
	 * @since   3.1.0 First time introduced.
	 */
	public function getUuidVersion() : ?int;
	
	/**
	 * Create UUID instance and return the string representation of the UUID.
	 * If you want to get version or any other information about UUID, you can use the following methods:
	 * $string = ($i = new Identifiers())->createUuid(1);
	 * $fields = $i->getFields()->getVersion(); // returns the version number (1-7)
	 *
	 * @param    int    $version
	 * @param           ...$args
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function createUuid(int $version, ...$args) : string;
	
	/**
	 * A version 1 UUID uses the current time, along with the MAC address (or node) for a network interface
	 * on the local machine. This serves two purposes:
	 * 1) You can know when the identifier was created.
	 * 2) You can know where the identifier was created.
	 * Optional parameters:
	 * $macAddress string The MAC address of the network interface.
	 *
	 * @param    string    $macAddress
	 * @param    int       $clockSequence
	 *
	 * @return UuidInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function createUuidV1(string $macAddress, int $clockSequence) : UuidInterface;
	
	/**
	 * Return uuid version 4 (random).
	 *
	 * @return UuidInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function createUuidV4() : UuidInterface;
	
	/**
	 * Return uuid version 5 (name-based SHA-1).
	 *
	 * @param    UuidInterface|string    $namespace
	 * @param    string                  $name
	 *
	 * @return UuidInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function createUuidV5(UuidInterface|string $namespace, string $name) : UuidInterface;
	
	/**
	 * Return uuid version 7 (timestamp-based).
	 * Optional parameters:
	 * $datetime DateTime The \DateTimeInterface object.
	 *
	 * @param    DateTimeInterface    $datetime
	 *
	 * @return UuidInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function createUuidV7(DateTimeInterface $datetime) : UuidInterface;
	
	/**
	 * Get the Crockford settings.
	 *
	 * @return null|Base32
	 * @since   3.1.0 First time introduced.
	 */
	public function getCrockfordSettings() : ?Base32;
	
	/**
	 * Set the Crockford settings.
	 *
	 * @param    Base32    $settings    The Base32 settings object.
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setCrockfordSettings(Base32 $settings) : void;
	
	/**
	 * Convert the given UUID to binary format.
	 *
	 * @param    string    $binary
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function binaryToUuid(string $binary) : string;
	
	/**
	 * Convert the given UUID to Crockford Base32 format.
	 *
	 * @param    UuidInterface    $uuid
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function convertUuidToBinary(UUidInterface $uuid) : string;
	
	/**
	 * Convert the given binary to ULID format.
	 *
	 * @param    string    $binary
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function binaryToUlid(string $binary) : string;
	
	/**
	 * Convert the given ULID to binary format.
	 *
	 * @param    string    $ulid    The ULID to convert.
	 *
	 * @return string The ULID converted to binary format.
	 * @since   3.1.0 First time introduced.
	 */
	public function convertUlidToBinary(string $ulid) : string;
}
