<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

//
use Tiat\Standard\Exception\InvalidArgumentException;

/**
 * Unofficial methods are supported like https://jantia.io/my/path?param1=value1:custom_method_name
 * All methods are mapped to official HTTP methods
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum HttpMethodCustom: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	// Unofficial methods
	
	/**
	 * Cancel/undelete an outstanding operation
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CANCEL = 'CANCEL';
	
	/**
	 * Clear operations
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CLEAR = 'CLEAR';
	
	/**
	 * Retrieve information about a specific resource or operation.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case INFO = 'INFO';
	
	/**
	 * Retrieve a list of resources or operations.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LIST = 'LIST';
	
	/**
	 * Lock a resource to prevent modification by other operations.
	 * Primarily used in systems with concurrent access, such as WebDAV.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LOCK = 'LOCK';
	
	/**
	 * Send mail or notifications related to an operation.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MAIL = 'MAIL';
	
	/**
	 * Partially update or modify an existing resource or operation.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PARTIAL = 'PARTIAL';
	
	/**
	 * Purge or invalidate a resource, often used to clear cache or temporary data.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PURGE = 'PURGE';
	
	/**
	 * Search for resources or operations that match specific criteria.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case SEARCH = 'SEARCH';
	
	/**
	 * Cancel/undelete an outstanding operation
	 *
	 * @since   3.0.0 First time introduced.
	 * @see     self::CANCEL
	 */
	final public const HttpMethodCustom UNDELETE = self::CANCEL;
	
	/**
	 * To use custom HTTP methods, you must map them to official HTTP methods using this method. If official methods
	 * will not match to this list then use of custom method is not supported.
	 *
	 * @param    HttpMethodCustom    $httpMethodCustom
	 *
	 * @return HttpMethod
	 * @since   3.0.0 First time introduced.
	 */
	public static function mapMethod(HttpMethodCustom $httpMethodCustom) : HttpMethod {
		return match ( $httpMethodCustom ) {
			self::INFO, self::LIST, self::PARTIAL, self::SEARCH => HttpMethod::GET,
			self::CANCEL, self::CLEAR, self::LOCK, self::MAIL, self::PURGE, self::UNDELETE => HttpMethod::POST,
			default => throw new InvalidArgumentException('To be implemented'),
		};
	}
	
	/**
	 * @param    HttpMethodCustom    $httpMethodCustom
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public static function getMessage(HttpMethodCustom $httpMethodCustom) : string {
		return match ( $httpMethodCustom ) {
			self::CANCEL, self::UNDELETE => 'Cancel/undelete an outstanding operation',
			self::CLEAR => 'Clear operations',
			self::INFO => 'Info about the current operation',
			self::MAIL => 'Send mail',
			self::LIST => 'List items',
			self::PARTIAL => 'Return only defined fields',
			self::SEARCH => 'Search items',
			self::PURGE => 'Remove items from cache',
			self::LOCK => 'Lock a resource, preventing other users from modifying it.',
			default => throw new InvalidArgumentException('To be implemented'),
		};
	}
}
