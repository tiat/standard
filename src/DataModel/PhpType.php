<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

//
use Tiat\Standard\Exception\InvalidArgumentException;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum PhpType: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ARRAY = 'array';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case BINARY = 'binary';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case BOOL = 'bool';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CALLABLE = 'callable';
	
	/**
	 * For historical reasons gettype() will return DOUBLE instead of FLOAT
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case DOUBLE = 'double';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FLOAT = 'float';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case INT = 'int';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ITERABLE = 'iterable';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case MIXED = 'mixed';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case NULL = 'null';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case OBJECT = 'object';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PARENT = 'parent';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case RESOURCE = 'resource';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SELF = 'self';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case STRING = 'string';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TIMESTAMP = 'timestamp';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TYPE_COMPOUND = 'compound';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TYPE_SCALAR = 'scalar';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TYPE_SINGLE = 'single';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TYPE_SPECIAL = 'special';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TYPE_TIME = 'time';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const PhpType DATETIME = self::TIMESTAMP;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const PhpType TYPE_MIXED = self::MIXED;
	
	/**
	 * @param    PhpType    $value
	 *
	 * @return PhpType[]
	 * @since   3.0.0 First time introduced.
	 */
	public static function byType(PhpType $value) : array {
		return match ( $value ) {
			self::TYPE_SINGLE => [self::SELF, self::PARENT, self::ARRAY, self::CALLABLE, self::BOOL, self::FLOAT,
			                      self::DOUBLE, self::INT, self::STRING, self::ITERABLE, self::OBJECT, self::MIXED],
			self::TYPE_MIXED => [self::OBJECT, self::ARRAY, self::STRING, self::INT, self::FLOAT, self::DOUBLE,
			                     self::BOOL, self::NULL, self::RESOURCE],
			self::TYPE_SCALAR => [self::BOOL, self::INT, self::FLOAT, self::STRING],
			self::TYPE_COMPOUND => [self::ARRAY, self::OBJECT],
			self::TYPE_SPECIAL => [self::NULL, self::RESOURCE],
			self::TYPE_TIME => [self::TIMESTAMP, self::DATETIME],
			default => throw new InvalidArgumentException("Value must be one of backed enum values or it's not supported.")
		};
	}
}
