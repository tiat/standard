<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

//
use Tiat\Standard\Exception\InvalidArgumentException;

use function is_int;
use function mb_strtolower;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitEnum {
	
	/**
	 * Convert backed enum values to array with string
	 *
	 * @param    array    $values
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public static function toArray(array $values) : ?array {
		if(! empty($values)):
			foreach($values as $val):
				$result[] = $val->value;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    string|int    $value
	 *
	 * @return static
	 * @since   3.0.0 First time introduced.
	 */
	public static function toValue(string|int $value) : static {
		if(is_int($value) && ( $e = self::tryFrom((string)$value) ) !== NULL):
			return $e;
		elseif(( $e = self::tryFrom(mb_strtolower($value, 'UTF-8')) ) !== NULL):
			return $e;
		else:
			throw new InvalidArgumentException(sprintf("Value '%s' is not supported", $value));
		endif;
	}
}
