<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum VariableElement: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case GET = 'GET';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case COOKIE = 'COOKIE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV = 'ENV';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FILES = 'FILES';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HEADERS = 'HEADERS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case POST = 'POST';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SERVER = 'SERVER';
}
