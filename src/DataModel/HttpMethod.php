<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

/**
 * Enum representing the various HTTP methods.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum HttpMethod: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CONNECT = 'CONNECT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DELETE = 'DELETE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case GET = 'GET';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HEAD = 'HEAD';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case OPTIONS = 'OPTIONS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PATCH = 'PATCH';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case POST = 'POST';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PUT = 'PUT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TRACE = 'TRACE';
}
