<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

//
use Tiat\Standard\Exception\InvalidArgumentException;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface InterfaceEnumString extends InterfaceEnum {
	
	/**
	 * @param    string    $value
	 *
	 * @return mixed
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public static function toValue(string $value) : mixed;
}
