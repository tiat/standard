<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Standard\DataModel;

//
use Tiat\Standard\Exception\InvalidArgumentException;

use function in_array;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum HttpMethodType: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * Safe HTTP methods.
	 * Safe methods are those that do not modify server state.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case SAFE = 'safe';
	
	/**
	 * Idempotent HTTP methods.
	 * Idempotent methods are those that can be called multiple times without different outcomes.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case IDEMPOTENCE = 'idempotence';
	
	/**
	 * HTTP methods that modify server resources.
	 * These methods are not safe and cause changes to the server state.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MODIFIES_RESOURCES = 'modifies_resources';
	
	/**
	 * CRUD based Delete
	 *
	 * @since   3.0.1 First time introduced.
	 */
	case CRUD_READ = 'crud_read';
	
	/**
	 * CRUD based Create operation.
	 * This case represents the action for inserting data using OAuth2 protocol.
	 *
	 * @since   3.0.1 First time introduced.
	 */
	case CRUD_CREATE = 'crud_create';
	
	/**
	 * CRUD based Update operation.
	 * This constant is used for operations related to updating OAuth2 credentials or settings.
	 *
	 * @since   3.0.1 First time introduced.
	 */
	case CRUD_UPDATE = 'crud_update';
	
	/**
	 * CRUD based Modify operation.
	 *
	 * @since   3.0.1 First time introduced.
	 */
	case CRUD_MODIFY = 'crud_modify';
	
	/**
	 * CRUD based Delete HTTP method.
	 * This method is typically used to delete a resource in a RESTful API.
	 *
	 * @since   3.0.1 First time introduced.
	 */
	case CRUD_DELETE = 'crud_delete';
	
	/**
	 * All http methods
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case VALID = 'valid';
	
	/**
	 * @param    HttpMethodType    $value
	 * @param    bool              $options    Add Http Method OPTIONS to the result array. Default is false.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 * @since   3.0.1 Added the CRUD parameters & add OPTIONS if not exists.
	 */
	public static function toMethod(HttpMethodType $value, bool $options = FALSE) : array {
		//
		$result = match ( $value ) {
			self::SAFE => [HttpMethod::GET, HttpMethod::HEAD, HttpMethod::OPTIONS, HttpMethod::TRACE
			               // TRACE is also considered a safe method, as it doesn't modify resources
			],
			self::CRUD_READ => [HttpMethod::GET, HttpMethod::HEAD, HttpMethod::OPTIONS],
			self::CRUD_CREATE => [HttpMethod::POST, HttpMethod::OPTIONS],
			self::CRUD_UPDATE => [HttpMethod::PUT, HttpMethod::PATCH, HttpMethod::OPTIONS],
			self::CRUD_MODIFY => [HttpMethod::POST, HttpMethod::PATCH, HttpMethod::PUT, HttpMethod::OPTIONS],
			self::CRUD_DELETE => [HttpMethod::DELETE, HttpMethod::OPTIONS],
			self::IDEMPOTENCE => [HttpMethod::DELETE, HttpMethod::GET, HttpMethod::HEAD, HttpMethod::OPTIONS,
			                      HttpMethod::PUT, HttpMethod::TRACE  // TRACE is also idempotent
			],
			self::MODIFIES_RESOURCES => [HttpMethod::POST, HttpMethod::PATCH, HttpMethod::PUT, HttpMethod::DELETE,],
			self::VALID => [HttpMethod::CONNECT, HttpMethod::DELETE, HttpMethod::GET, HttpMethod::HEAD,
			                HttpMethod::OPTIONS, HttpMethod::PATCH, HttpMethod::POST, HttpMethod::PUT, HttpMethod::TRACE
			                // Include TRACE and CONNECT as valid HTTP methods
			],
			default => throw new InvalidArgumentException('Invalid HttpMethodType provided.'),
		};
		
		// Add Http Method OPTIONS if requested
		if($options === TRUE && $value !== self::VALID && ! in_array(HttpMethod::OPTIONS, $result, TRUE)):
			$result[] = HttpMethod::OPTIONS;
		endif;
		
		//
		return $result;
	}
}
